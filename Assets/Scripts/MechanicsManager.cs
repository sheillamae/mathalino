﻿using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// MechanicsManager Class
/// This class handles all the mechanics of the game.
/// - Timer: how the timer behaves
/// - Correct/Wrong: counts the number of corrects and wrongs
/// - Answer: how the answer behaves when a button is pressed
/// - Equation: how the equation behaves when the AI decides
/// </summary>
public sealed class MechanicsManager : MonoBehaviour
{
    /*
     * @Singleton definition of the class
     */
    private static MechanicsManager _instance = null;
    public static MechanicsManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType(typeof(MechanicsManager)) as MechanicsManager;

            if (_instance == null)
            {
                GameObject obj = new GameObject("MechanicsManager");
                _instance = obj.AddComponent(typeof(MechanicsManager)) as MechanicsManager;

                Debug.Log("Could not locate MechanicsManager object. MechanicsManager was Generated Automatically.");
            }

            return _instance;
        }
    }

    ///
    /// Timer Game Object functions 
    /// 

    /*
     * @Properties
     * @Private
     */
    private float _seconds = 0.0f;
    private int _minutes = 0;

    /*
     * @Accessors/Getters
     */
    public int GetSeconds() { return (int)_seconds; }
    public int GetMinutes() { return _minutes; }

    /*
     * @Functions
     */
    /// <summary>
    /// This Update Function call is part of Unity Monobehaviour
    /// It is called every frame update by Unity
    /// </summary>
    void Update()
    {
        _seconds += Time.deltaTime;

        if (_seconds >= 60.0f)
        {
            _seconds = 0.0f;
            _minutes++;
        }
    }

    public void ResetTime()
    {
        _seconds = 0.0f;
        _minutes = 0;
    }

    ///
    /// Answer Text Game Object functions
    ///

    /*
     * @Properties
     * @Private
     */
    private string _textForAnswerObject = "0";

    /*
     * @Accessors/Getters
     */
    public string GetTextForAnswerObject() { return _textForAnswerObject; }
    
    /*
     * @Functions
     */
    /// <summary>
    /// Adds a string to the end of the answer
    /// </summary>
    public void AddString(string str)
    {
        /// check for '0' in front
        if (_textForAnswerObject[0] == '0')
            _textForAnswerObject = "";

        /// check for maximum length of answer
        if (_textForAnswerObject.Length > 9)
            return;

        _textForAnswerObject += str;
    }

    /// <summary>
    /// Deletes a string at the end of the answer
    /// </summary>
    public void SubtractString()
    {
        if (_textForAnswerObject.Length <= 1)
        {
            _textForAnswerObject = "0";
            return;
        }

        int endIndex = _textForAnswerObject.Length - 1;
        _textForAnswerObject = _textForAnswerObject.Remove(endIndex);
    }

    /// <summary>
    /// Clears the answer
    /// </summary>
    public void ClearAnswerString()
    {
        _textForAnswerObject = "0";
    }

    ///
    /// Equation Text Game Object functions
    /// 

    /*
     * @Properties
     * @Private
     */
    private float _leftHandSide;
    private float _rightHandSide;

    public enum enumOperation
    {
        Add = 0,
        Sub = 1,
        Mul = 2,
        Div = 3
    };

    private enumOperation _eOperation = enumOperation.Add;
    private bool _isAnswerCorrect = false;
    private int _numberOfCorrectAnswers = 0;
    private int _numberOfWrongAnswers = 0;


    /*
     * @Accessors/Getters
     */
    public float GetLeftHandSide() { return _leftHandSide; }
    public float GetRightHandSide() { return _rightHandSide; }
    public enumOperation GetOperation() { return _eOperation; }
    public int NumberOfCorrectAnswers { get { return _numberOfCorrectAnswers; } }
    public int NumberOfWrongAnswers { get { return _numberOfWrongAnswers; } }
    
    /*
     * @Functions
     */
    /// <summary>
    /// Checks the equation with the answer if its correct or not
    /// It then updates the player model
    /// And after gets the next equation from the difficulty balancer
    /// </summary>
    public void CheckEquationWithAnswer()
    {
        float answer = 0.0f;
        bool canAnswerBeParsed = float.TryParse(_textForAnswerObject, out answer);
        if (!canAnswerBeParsed)
            Debug.LogError("Cannot Parse Answer!");

        float result = 0.0f;
        switch (_eOperation)
        {
            case enumOperation.Add: 
                result = _leftHandSide + _rightHandSide;
                break;
            case enumOperation.Sub:
                result = _leftHandSide - _rightHandSide;
                break;
            case enumOperation.Mul:
                result = _leftHandSide * _rightHandSide;
                break;
            case enumOperation.Div:
                result = _leftHandSide / _rightHandSide;
                break;
        }

        // if answer is correct
        if (answer == result)
        {
            _isAnswerCorrect = true;
            _numberOfCorrectAnswers++;
        }
        // if answer is wrong
        else
        {
            _isAnswerCorrect = false;
            _numberOfWrongAnswers++;
        }

        ClearAnswerString();

        /// player model update and equation generation here
        PlayerModel.Instance.UpdatePlayerModel(_isAnswerCorrect);
        GetNextEquation();

        ResetTime();
    }

    /// <summary>
    /// Shuffles the Equation.
    /// This is where the difficulty balancing AI will begin to check 
    /// Then change the difficulty of the equation
    /// </summary>
    public void GetNextEquation()
    {
        DifficultyBalancer.Instance.BalanceDifficulty();

        _leftHandSide = Mathf.Floor (UnityEngine.Random.Range (21.0f, 30.0f));
        _rightHandSide = Mathf.Floor (UnityEngine.Random.Range(1.0f, 20.0f));
        _eOperation = (enumOperation)UnityEngine.Random.Range(0, 3);
    }
}
