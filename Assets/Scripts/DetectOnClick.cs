﻿using UnityEngine;
using System.Collections;

public class DetectOnClick : MonoBehaviour
{
    public Camera _camera;

    void Update()
    {
        Ray ray;
        RaycastHit hit;

        if (Input.GetMouseButtonDown(0))
        {
            ray = _camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                hit.transform.gameObject.SendMessage("OnLeftClick", hit.point, SendMessageOptions.DontRequireReceiver);
            }
        }
    }
}
