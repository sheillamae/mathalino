﻿using UnityEngine;
using System.Collections;

public class ButtonBackspace : MonoBehaviour {

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Application.CaptureScreenshot("screen");
        }
    }

    void OnLeftClick()
    {
        MechanicsManager.Instance.SubtractString();
    }

    void OnTouchBegan()
    {
        MechanicsManager.Instance.SubtractString();
    }
}
