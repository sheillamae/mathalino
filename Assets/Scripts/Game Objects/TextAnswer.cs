﻿using UnityEngine;
using System.Collections;

public class TextAnswer : MonoBehaviour {

	void Update ()
    {
        this.GetComponent<TextMesh>().text = MechanicsManager.Instance.GetTextForAnswerObject();
	}
}
