﻿using UnityEngine;
using System.Collections;

public class ButtonOK : MonoBehaviour
{
    void OnLeftClick()
    {
        MechanicsManager.Instance.CheckEquationWithAnswer();
    }

    void OnTouchBegan()
    {
        MechanicsManager.Instance.CheckEquationWithAnswer();
    }
}
