﻿using UnityEngine;
using System.Collections;

public class HUDTextCorrect : MonoBehaviour
{
	void Update ()
    {
        this.GetComponent<TextMesh>().text = MechanicsManager.Instance.NumberOfCorrectAnswers.ToString();
	}
}
