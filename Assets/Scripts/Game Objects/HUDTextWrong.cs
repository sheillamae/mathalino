﻿using UnityEngine;
using System.Collections;

public class HUDTextWrong : MonoBehaviour {

    void Update()
    {
        this.GetComponent<TextMesh>().text = MechanicsManager.Instance.NumberOfWrongAnswers.ToString();
    }
}
