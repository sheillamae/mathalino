﻿using UnityEngine;
using System.Collections;

public class TextEquation : MonoBehaviour
{
	void Start ()
    {
        MechanicsManager.Instance.GetNextEquation();
	}

    void Update()
    {
        string operation = "";
        switch(MechanicsManager.Instance.GetOperation())
        {
            case MechanicsManager.enumOperation.Add: operation = " + "; break;
            case MechanicsManager.enumOperation.Sub: operation = " - "; break;
            case MechanicsManager.enumOperation.Mul: operation = " * "; break;
            case MechanicsManager.enumOperation.Div: operation = " / "; break;
        }

        string equation =
            MechanicsManager.Instance.GetLeftHandSide().ToString() +
            operation +
            MechanicsManager.Instance.GetRightHandSide().ToString();

        this.GetComponent<TextMesh>().text = equation;
    }
}
