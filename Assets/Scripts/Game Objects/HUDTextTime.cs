﻿using UnityEngine;
using System.Collections;

public class HUDTextTime : MonoBehaviour {

	void Update()
    {
        this.GetComponent<TextMesh>().text =
            MechanicsManager.Instance.GetMinutes().ToString().PadLeft(2, '0') + ":" +
            MechanicsManager.Instance.GetSeconds().ToString().PadLeft(2, '0');
	}
}
