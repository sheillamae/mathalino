﻿using UnityEngine;
using System;
using System.Collections;

public class ButtonKeypad : MonoBehaviour
{
    public int _btnID = 0;

    void Awake()
    {
        /// to make keypad buttons colorful
        this.GetComponent<SpriteRenderer>().color = new Color(
            UnityEngine.Random.Range(0.5f, 1.0f),
            UnityEngine.Random.Range(0.5f, 1.0f),
            0.5f);
    }

    void OnLeftClick()
    {
        AddToMainString();
    }

    void OnTouchBegan()
    {
        AddToMainString();
    }

    void AddToMainString()
    {
        int endIndex = this.gameObject.name.Length - 1;

        try
        {
            _btnID = int.Parse(this.gameObject.name[endIndex].ToString());
        }
        catch (System.Exception e)
        {
            _btnID = 0;

            Debug.Log(e);
            throw new ApplicationException("Cannot Parse endIndex :", e);
        }

        Debug.Log("you clicked number " + this.gameObject.name[endIndex] + "!!!!!~");

        MechanicsManager.Instance.AddString(_btnID.ToString());
    }
}