﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// DifficultyBalancer Class
/// This class handles the balancing of difficulty by using rule-based system
/// It also handles what equations will be answered next by the player
/// </summary>
public sealed class DifficultyBalancer : MonoBehaviour 
{
    /*
     * @Singleton definition of the class
     */
    private static DifficultyBalancer _instance = null;
    public static DifficultyBalancer Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType(typeof(DifficultyBalancer)) as DifficultyBalancer;

            if (_instance == null)
            {
                GameObject obj = new GameObject("DifficultyBalancer");
                _instance = obj.AddComponent(typeof(DifficultyBalancer)) as DifficultyBalancer;

                Debug.Log("Could not locate DifficultyBalancer object. DifficultyBalancer was Generated Automatically.");
            }

            return _instance;
        }
    }

    /*
     * @Properties
     * @Private
     */
    private List<Rule> _listOfRules = new List<Rule>();
    private Knowledge _knowledgeDatabase = new Knowledge();
    private Arbiter _arbiter = new Arbiter();

    /*
     * @Functions
     */
    /// <summary>
    /// This Start Function call is part of Unity Monobehaviour
    /// It is called everytime the game starts by Unity
    /// </summary>
    void Start()
    {
        // setting the rules here
        // if PLAYERTRAIT is between VALUEFROM and VALUETO then call action/function DIFFICULTYLEVEL
        _listOfRules.Add(new Rule(enumPlayerTraits.PlayerSkillLevel, 0.0f, 0.2f, DifficultyLevel1));
        _listOfRules.Add(new Rule(enumPlayerTraits.PlayerSkillLevel, 0.2f, 0.4f, DifficultyLevel2));
        _listOfRules.Add(new Rule(enumPlayerTraits.PlayerSkillLevel, 0.4f, 0.6f, DifficultyLevel3));
        _listOfRules.Add(new Rule(enumPlayerTraits.PlayerSkillLevel, 0.6f, 0.8f, DifficultyLevel4));
        _listOfRules.Add(new Rule(enumPlayerTraits.PlayerSkillLevel, 0.8f, 1.0f, DifficultyLevel5));
    }

    public void BalanceDifficulty()
    {
        // add skill level value to knowledge database
        _knowledgeDatabase.AddKnowledge(enumPlayerTraits.PlayerSkillLevel, PlayerModel.Instance.GetPlayerSkillLevelNode().GetTraitValue());

        // call arbiter passing knowledge and rules as parameters
        _arbiter.Decide(_listOfRules, _knowledgeDatabase);
    }

    ///
    /// Difficulty Level Functions for Equation Generation - currently empty still
    ///

    public void DifficultyLevel1(object sender, EventArgs e)
    {
        // get level 1 difficulty equation
        Debug.Log("DiffLevel: 1");
    }

    public void DifficultyLevel2(object sender, EventArgs e)
    {
        // get level 2 difficulty equation
        Debug.Log("DiffLevel: 2");
    }

    public void DifficultyLevel3(object sender, EventArgs e)
    {
        // get level 3 difficulty equation
        Debug.Log("DiffLevel: 3");
    }

    public void DifficultyLevel4(object sender, EventArgs e)
    {
        // get level 4 difficulty equation
        Debug.Log("DiffLevel: 4");
    }

    public void DifficultyLevel5(object sender, EventArgs e)
    {
        // get level 5 difficulty equation
        Debug.Log("DiffLevel: 5");
    }
}
