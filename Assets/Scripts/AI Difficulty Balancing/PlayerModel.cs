﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// PlayerTrait Enumeration
/// enumerates all player traits in the game
/// </summary>
public enum enumPlayerTraits
{
    PlayerAnswered = 0,
    PlayerTimeTaken = 1,
    PlayerSkillLevel = 2
}

/// <summary>
/// PlayerModel Class
/// Handles initialization of Player Model Nodes with PlayerTraits as their ID's
/// It also handles the updating of the Player Model when the player does something
/// in the game.
/// 
/// The LEARNING_RATE determines how quick the AI will learn about the traits of the player ingame
/// This class is linked with the rule-based system classes on the DifficultyBalancer class
/// </summary>
public sealed class PlayerModel : MonoBehaviour
{
    /*
     * @Singleton definition of the class
     */
    private static PlayerModel _instance = null;
    public static PlayerModel Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType(typeof(PlayerModel)) as PlayerModel;

            if (_instance == null)
            {
                GameObject obj = new GameObject("PlayerModel");
                _instance = obj.AddComponent(typeof(PlayerModel)) as PlayerModel;

                Debug.Log("Could not locate PlayerModel object. PlayerModel was Generated Automatically.");
            }

            return _instance;
        }
    }

    /*
     * @Properties
     * @Private
     */
    private float LEARNING_RATE = 0.1f;
    private PlayerModelNode _rootNode;

    /*
     * @Accessors/Getters
     */
    public float GetLearningRate() { return LEARNING_RATE; }
    public PlayerModelNode GetPlayerSkillLevelNode() { return _rootNode; }
    public PlayerModelNode GetPlayerAnsweredNode() { return _rootNode.GetChildren()[enumPlayerTraits.PlayerAnswered]; }
    public PlayerModelNode GetPlayerTimeTakenNode() { return _rootNode.GetChildren()[enumPlayerTraits.PlayerTimeTaken]; }

    /*
     * @Functions
     */
    /// <summary>
    /// This Start Function call is part of Unity Monobehaviour
    /// It is called everytime the game starts by Unity
    /// </summary>
    void Start()
    {
        _rootNode = new PlayerModelNode(enumPlayerTraits.PlayerSkillLevel);
        _rootNode.AddChild(new PlayerModelNode(enumPlayerTraits.PlayerAnswered));
        _rootNode.AddChild(new PlayerModelNode(enumPlayerTraits.PlayerTimeTaken));
    }

    ///
    /// Player Model Update Function
    /// 

    /*
     * @Properties
     * @Private
     */
    private float _pModelCorrectAnswerValue = 0.0f;
    private float _pModelWrongAnswerValue = 0.0f;
    private float _playerModelMinValue = 0.1f;
    private float _playerModelMaxValue = 0.6f;
    private int _secondsToAnswer = 5;

    /*
     * @Functions
     */
    /// <summary>
    /// Player Model Update
    /// everything that needs to be updated in the player model
    /// </summary>
    public void UpdatePlayerModel(bool isAnswerCorrect)
    {
        /// update the PLAYER ANSWER TRAIT
        if (isAnswerCorrect)
        {
            _pModelWrongAnswerValue = 0.0f;
            _pModelCorrectAnswerValue += _playerModelMinValue;

            // limit check for greater than 0.6
            if (_pModelCorrectAnswerValue >= _playerModelMaxValue)
                _pModelCorrectAnswerValue = _playerModelMaxValue;

            // add to PLAYER MODEL TRAIT (correctAnswer) here
            GetPlayerAnsweredNode().UpdateNode(_pModelCorrectAnswerValue);

            /// update the PLAYER TIME TAKEN TRAIT
            if (MechanicsManager.Instance.GetSeconds() <= _secondsToAnswer)
            {
                GetPlayerTimeTakenNode().UpdateNode(0.2f);
            }
            else
            {
                GetPlayerTimeTakenNode().UpdateNode(-0.2f);
            }
        }
        else
        {
            _pModelCorrectAnswerValue = 0.0f;
            _pModelWrongAnswerValue += -_playerModelMinValue;

            // limit check for less than -0.6
            if (_pModelWrongAnswerValue <= -_playerModelMaxValue)
                _pModelWrongAnswerValue = -_playerModelMaxValue;

            // add to PLAYER MODEL TRAIT (wrongAnswer) here
            GetPlayerAnsweredNode().UpdateNode(_pModelWrongAnswerValue);
        }

        Debug.Log("PlayerModel.Instance.RootNode.TraitValue: " + PlayerModel.Instance.GetPlayerSkillLevelNode().GetTraitValue());
    }
}

/// <summary>
/// Class PlayerModelNode
/// Handles calculations of the nodes defined in PlayerModel
/// </summary>
public class PlayerModelNode
{
    /*
     * @Properties
     * @Private
     */
    private Dictionary<enumPlayerTraits, PlayerModelNode> _children = new Dictionary<enumPlayerTraits, PlayerModelNode>();
    private PlayerModelNode _parent;
    private enumPlayerTraits _trait;
    private float _traitValue;

    /*
     * @Accessors/Getters
     */
    public Dictionary<enumPlayerTraits, PlayerModelNode> GetChildren() { return _children; }
    public float GetTraitValue()
    {
        float result = 0.0f;

        if (_traitValue < 0.0f)
        {
            result = 0.0f;
        }
        else if (_traitValue > 1.0f)
        {
            result = 1.0f;
        }
        else
        {
            result = _traitValue;
        }

        _traitValue = result;

        return result;
    }

    /*
     * @Functions
     */
    public PlayerModelNode(enumPlayerTraits trait)
    {
        _trait = trait;
        _traitValue = 0.1f;
    }

    public void AddChild(PlayerModelNode node)
    {
        node._parent = this;
        _children.Add(node._trait, node);
    }

    public void UpdateNode(float observedValue)
    {
        float delta = observedValue - _traitValue;
        float weightedDelta = PlayerModel.Instance.GetLearningRate() * delta;
        float newValue = _traitValue + weightedDelta;

        if (_parent != null)
        {
            _parent.Propagate(_traitValue, newValue);
        }

        _traitValue = newValue;
    }

    public void Propagate(float oldContribution, float newContribution)
    {
        int numChild = _children.Count;
        float numberOfContribution = _traitValue * numChild - oldContribution;
        float newValue = (numberOfContribution + newContribution) / numChild;

        if (_parent != null)
        {
            _parent.Propagate(_traitValue, newValue);
        }

        _traitValue = newValue;
    }
}
