﻿using System;
using System.Collections;

/// <summary>
/// Rule Class
/// Handles the rules of the rule-based system
/// It has a condition (if _playerTrait is between _valueFrom and _valueTo)
/// It has an action (then ActivateAction)
/// </summary>
public class Rule
{
    /*
     * @Properties
     * @Private
     */
    public event EventHandler _ruleAction;
    private enumPlayerTraits _playerTrait;
    private float _valueFrom;
    private float _valueTo;

    /*
     * @Accessors/Getters
     */
    public enumPlayerTraits GetPlayerTrait() { return _playerTrait; }
    public float GetValueFrom() { return _valueFrom; }
    public float GetValueTo() { return _valueTo; }

    /*
     * @Functions
     */
    public Rule(enumPlayerTraits playerTrait, float valueFrom, float valueTo, EventHandler action)
    {
        _playerTrait = playerTrait;
        _valueFrom = valueFrom;
        _valueTo = valueTo;
        _ruleAction = action;
    }

    public void ActivateAction(EventArgs e)
    {
        if (_ruleAction != null)
        {
            _ruleAction(this, e);
        }
    }
}
