﻿using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Knowledge Class
/// Handles the knowledge known to the AI
/// It has a dictionary as a container with player traits as ID
/// and a float value of the that trait e.g.(PlayerSkillLevel, 0.2)
/// </summary>
public class Knowledge
{
    /*
     * @Properties
     * @Private
     */
    private Dictionary<enumPlayerTraits, float> _knowledgeBase = new Dictionary<enumPlayerTraits, float>();

    /*
     * @Accessors/Getters
     */
    public Dictionary<enumPlayerTraits, float> GetContainer() { return _knowledgeBase; }

    /*
     * @Functions
     */
    public Knowledge() { }

    public void AddKnowledge(enumPlayerTraits key, float value)
    {
        if (_knowledgeBase.ContainsKey(key))
        {
            _knowledgeBase[key] = value;
        }
        else
        {
            _knowledgeBase.Add(key, value);
        }
    }

    public void ClearKnowledge() { _knowledgeBase.Clear(); }
}
