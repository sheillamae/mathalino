﻿using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Arbiter Class
/// Handles the decision on which of the rules have met their condition
/// If there are any then the associated action/function is activated
/// </summary>
public class Arbiter
{
    /*
     * @Functions
     */
    public Arbiter() { }

    public void Decide(List<Rule> ruleList, Knowledge knowledge)
    {
        foreach (Rule r in ruleList)
        {
            if (knowledge.GetContainer().ContainsKey(r.GetPlayerTrait()))
            {
                float temp = knowledge.GetContainer()[r.GetPlayerTrait()];

                if (temp > r.GetValueFrom() && temp <= r.GetValueTo())
                {
                    r.ActivateAction(EventArgs.Empty);
                }
            }
        }
    }
}
