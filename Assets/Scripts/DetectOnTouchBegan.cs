﻿using UnityEngine;
using System.Collections;

public class DetectOnTouchBegan : MonoBehaviour
{
    public Camera _camera;

    void Update()
    {
        Ray ray;
        RaycastHit hit;

        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    ray = _camera.ScreenPointToRay(touch.position);

                    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                    {
                        hit.transform.gameObject.SendMessage("OnTouchBegan", hit.point, SendMessageOptions.DontRequireReceiver);
                    }
                }
            }
        }
    }
}
